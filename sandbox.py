
eval_allowed_names = {
    "sum": sum
}

class EvaluationContext(object):
    pass


# @see https://realpython.com/python-eval-function/
def execute_expression(input_string, context_holder):
    if isinstance(context_holder, EvaluationContext):
        context_holder = context_holder.__dict__
    code = compile(input_string, "<string>", "eval")
    allowed_locals = {**eval_allowed_names, **context_holder}
    # for name in code.co_names:
    #     if name not in allowed_locals:
    #         raise NameError(f"Use of {name} not allowed")
    return eval(code, {"__builtins__": {}}, allowed_locals)
