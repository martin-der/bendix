from maubot.handlers import web
from aiohttp import web as aioweb
from aiohttp.web import Request, Response, json_response
import aiohttp
import os.path
from resources import Resources
import html

def to_html(value):
    if value is None:
        return '<img class="value-icon nul-value" src="../~/icon/google/not_interested.svg"/>'
    elif isinstance(value, bool):
        return '<img class="value-icon boolean-value boolean-'+('true' if value else 'false')+'-value" src="../~/icon/google/'+('check_circle' if value else 'highlight_off')+'.svg"/>'
    else:
        return html.escape(value) if isinstance(value, str) else html.escape(str(value))

def element_to_html(value, html: str = None):
    if html is None:
        html = ''
    if isinstance(value, dict):
        return dict_to_html(value, html)
    if isinstance(value, list):
        return list_to_html(value, html)
    else:
        return to_html(value)

def list_to_html(content: list, html: str = None):
    if html is None:
        html = ''
    html2 = html + '<ul class="type-sequence">\n'
    for value in content:
        html2 = html2 + '<li>'
        html2 = html2 + element_to_html(value)
        html2 = html2 + '</li>\n'
    html2 = html2 + '</ul>\n'
    return html2

def dict_to_html(content: dict, html: str = None):
    if html is None:
        html = ''
    html2 = html + '<ul  class="type-dictionary">\n'
    for key, value in content.items():
        html2 = html2 + '<li>'
        # html2 = html2 + '<img src="../~/icon/google/key.svg"/>'
        html2 = html2 + '<span class="key">' + to_html(key) + '</span>'
        html2 = html2 + element_to_html(value)
        html2 = html2 + '</li>\n'
    html2 = html2 + '</ul>\n'
    return html2

def generic_response(request: Request, title: str = '', body: dict = None, status: int = 200):
    if request.content_type == 'application/json':
        return json_response({'title': title, 'status': status, 'content': {} if body is None else body})
    else:
        html_dict = '' if body is None else dict_to_html(body)
        content = f"""
        <!DOCTYPE html>
        <html>
            <head>
                <title>{title}</title>
                <style>""" + """
                    :root {
                      --list-element-spacing: 4px;
                      --list-element-border-size: 2px;
                    }
                    body {
                         margin: 0.3em;
                    }
                    ul {
                        list-style-type: none;
                    }
                    span.key {
                        margin-right: 1em;
                        border: solid 1px grey;
                        border-radius: 3px;
                        padding-left: 0.2em;
                        padding-right: 0.2em;
                    }
                    h1 {
                        text-align: center;
                        background-color: #03033a;
                        color: white;
                        border-radius: 0.2em;
                    }
                    
                    img.value-icon  {
                        height: 0.9em;
                        vertical-align: middle;
                    }
                    img.boolean-true-value {
                        filter: invert(.5) sepia(1) saturate(5) hue-rotate(73deg);
                    }
                    img.boolean-false-value {
                        filter: invert(.5) sepia(1) saturate(5) hue-rotate(284deg);
                    }
                    
                    p#content>ul,
                    p#content+ul {
                        display: inline-block;
                    }
                
                    ul.type-sequence>li>ul.type-dictionary {
                        padding-left: 0px;
                    }                    
                    ul.type-sequence {
                        margin: 4px;
                    }
                    ul.type-sequence>li:not(:last-child) {
                        padding-bottom: var(--list-element-spacing);
                    }
                    ul.type-sequence>li:not(:first-child) {
                        border-top: solid var(--list-element-border-size) black;
                        padding-top: var(--list-element-spacing);
                    }
                </style>""" + f"""
            </head>
            <body>
                <h1>{status} | {title}</h1>
                <p id="content">{html_dict}</p>
            </body>
        </html>
        """
        return Response(content_type='text/html', reason=title, status=status, body=content, charset='UTF-8')


class WebResourcesHandler:

    @staticmethod
    def get_or_return_404(original_path: str, resource_path: str, content_type: str = None) \
            -> Response:
    
        if content_type is None:
            extension = os.path.splitext(resource_path)[1][1:]
            content_type = Resources.content_type(extension)
    
        try:
            resource = Resources.string(f"web/{resource_path}")
        except BaseException as e:
            return aioweb.HTTPNotFound(reason=f"Resource '{original_path}' not found in '{resource_path}' : {e}")
    
        return Response(content_type=content_type, charset='UTF-8', body=resource)

    @web.get("/~/{path:.+}")
    async def static_resource(self, request: Request) -> Response:
    
        full_path = request.match_info.get("path")
    
        if full_path.startswith('js/'):
            return WebResourcesHandler.get_or_return_404(full_path, f"js/{full_path[3:]}", 'application/javascript')
    
        if full_path.startswith('icon/'):
            if full_path.endswith('.svg'):
                return WebResourcesHandler.get_or_return_404(full_path, full_path)
    
        if full_path.startswith('font/'):
            if full_path.startswith('font/fontawesome'):
                return WebResourcesHandler.get_or_return_404(full_path, full_path)
            else:
                return WebResourcesHandler.get_or_return_404(full_path, full_path)
    
        if full_path.startswith('css/'):
            return WebResourcesHandler.get_or_return_404(full_path, f"css/{full_path[4:]}", 'text/css')
    
        return aioweb.HTTPNotFound(reason=f"Resource '{full_path}' not found")
