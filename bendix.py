from mautrix.types import EventType, TextMessageEventContent, MessageType, Format, RelatesTo, RelationType, ReactionEvent
from maubot import Plugin, MessageEvent
from maubot.handlers import event, web
from typing import Type
from mautrix.util.config import BaseProxyConfig, ConfigUpdateHelper
from aiohttp.web import Request, Response, json_response
from admin import AdminHandler
from web import WebResourcesHandler
from circuit import Data, CPU
from config import load_config
from bendixlogger import BendixLogger


class BendixBot(Plugin):

    data: Data

    cpu: CPU

    debug: {}

    bendixLog: BendixLogger

    class Config(BaseProxyConfig):

        bot: object

        def do_update(self, helper: ConfigUpdateHelper) -> None:
            helper.copy("rooms")
            helper.copy("rights")
            helper.copy_dict("users")
            helper.copy("events")
            helper.copy("actuators")
            helper.copy_dict("actions")

    @classmethod
    def get_config_class(cls) -> Type[BaseProxyConfig]:
        return cls.Config

    async def start(self) -> None:

        self.bendixLog = BendixLogger(self.client, self.log)
        self.config.bot = self
        self.config.log = self.bendixLog
        self.config.load_and_update()
        self.data = Data()

        try:
            load_config(self, self.config)
        except Exception as ex:
            self.log.error(f"Failed to load config : {ex}")

        if 'log' in self.debug:
            if 'room' in self.debug['log']:
                room_id = self.debug['log']['room']
                room_mxid = self.data.get_room_mxid_by_id(room_id)
                if room_mxid is None:
                    self.log.warning(f"No such room with id = '{room_id}'")
                else:
                    self.bendixLog.rooms.append(room_mxid)

        self.cpu = CPU(self.data, self.client, self.debug, self.log)
        self.register_handler_class(WebResourcesHandler())
        self.register_handler_class(AdminHandler(self.data, self.debug))

        await self.bendixLog.info("I'm ready")

    @event.on(EventType.REACTION)
    async def reaction_handler(self, event: ReactionEvent) -> None:

        await self.cpu.handle_room_reaction(event)


    @event.on(EventType.ROOM_MESSAGE)
    async def message_handler(self, event: MessageEvent) -> None:

        await self.cpu.handle_room_message(event)

    @web.get("/event/{event:.+}")
    async def hook_event(self, request: Request) -> Response:
        event = request.match_info.get("event", "")
        response = await self.cpu.handle_webhook_event(event, request)
        return response if response is not None else Response(reason='Acknowledged', body='')
