from enum import Enum
import re
import shlex
from mautrix.types import EventType, TextMessageEventContent, MessageType, Format, RelatesTo, RelationType, ReactionEvent
from maubot import MessageEvent
from aiohttp.web import Request, Response, json_response, UrlDispatcher
from aiohttp.web_exceptions import HTTPNotFound, HTTPMethodNotAllowed
from web import generic_response
from maubot.client import MaubotMatrixClient
from sandbox import execute_expression, EvaluationContext
from mother_board import Data
import asyncio


class CircuitException(Exception):
    context = None

    def __init__(self, message: str, context):
        self.id = id
        self.context = context


class Event:
    class Type(Enum):
        WEB_HOOK = 1
        MATRIX_MESSAGE_REGEX = 2
        MATRIX_COMMAND = 3
        MATRIX_REACTION = 4

    id = None

    type: Type

    method: str

    webhook_path: str

    matrix_command: str

    matrix_command_parameters: []

    matrix_message_matcher: str

    matrix_reaction: []

    triggers: []

    def __init__(self, id: str, type: Type):
        self.id = id
        self.type = type
        self.method = None
        self.webhook_path = None
        self.matrix_command = None
        self.matrix_command_parameters = []
        self.matrix_message_matcher = None
        self.matrix_reaction = None
        self.triggers = []

    class Invocation:

        class Arguments(object):
            _full_line: str
            _indexed: []
            _named: {}

            def __init__(self, full_line: str, indexed: [], named: {}):
                self._full_line = full_line
                self._indexed = indexed
                self._named = {}

            def __getitem__(self, key):
                if isinstance(key, str):
                    return self._named[key]
                elif isinstance(key, int):
                    return self._indexed[key]
                else:
                    raise ValueError(f"Not a valid key type {type(k).__name__}")

            @property
            def full_line(self):
                return self._full_line

        prototype: object

        room: str

        sender: str

        event_message: object

        command_arguments: object

        def __init__(self, prototype: object):
            self.prototype = prototype
            self.room = None
            self.sender = None
            self.event_message = None
            self.command_arguments = None

    def invocation(self):
        return self.Invocation(self)

class Actuator:
    id: str

    name: str

    requisites: []

    action: object

    injectors: {}

    def __init__(self, id: str):
        self.id = id
        self.name = None
        self.action = None
        self.requisites = []
        self.injectors = {}

class Action:
    class Parameter:

        name: str
        kind: str
        mandatory: bool
        automatic_context: str
        evaluate: bool

        def __init__(self, name: str, kind: str = 'string', mandatory: bool = False, automatic_context: str = None, evaluate: bool = False):
            self.name = name
            self.kind = kind
            self.mandatory = mandatory
            self.automatic_context = automatic_context
            self.evaluate = evaluate

    class Command:

        kind: str

        content: str

        def __init__(self, kind: str):
            self.kind = kind
            self.arguments = {}

        def execute(self):
            raise "Execute hasn't been implemented"

    class HttpRequestCommand(Command):
        url: str
        method: str

        def __init__(self, url: str, method: str = 'GET'):
            super().__init__('http-request')
            self.url = url
            self.method = method

    class MatrixRoomMessageCommand(Command):
        room: str
        user: str
        relate_to_event: str
        message: str


        def __init__(self):
            super().__init__('matrix-room-message')
            self.message = None
            self.room = None
            self.user = None
            self.relate_to_event = None


    id: str

    name: str

    requirements: []

    parameters: []

    commands: []

    enabled: bool

    def __init__(self, id: str):
        self.id = id
        self.name = None
        self.enabled = True
        self.requirements = []
        self.parameters = []
        self.commands = []


class Chain:

    class Mapper:
        target: str
        expression: str

    mappers: dict

    def __init__(self, action: object):
        self.mappers = {}
        self.action = Action()


class Context:
    room_id = None


class UserInformation:
    def __init__(self):
        self.name = None


class Service:
    data = None

    def __init__(self, data: Data):
        if data is None:
            raise AssertionError("data cannot be None")
        self.data = data

async def handle_request(cpu, request: Request):
    match_info = await cpu.urlDispatcher.resolve(request)
    expression = match_info.get('expression')
    try:
        result = execute_expression(match_info.get('expression'))
    except Exception as e:
        return generic_response(request, title='Invalid expression', status=400, body={'expression': expression, 'error': str(e)})
    return generic_response(request, title='Result', body={'expression': expression, 'value': result})

class CPU:
    data = None
    log = None
    urlDispatcher: UrlDispatcher
    pathPrefix = '/event'
    client: MaubotMatrixClient

    COMMAND_SPLITER = "[^\s\"']+|\"([^\"]*)\"|'([^']*)'"

    def __init__(self, data: Data, matrix_client: MaubotMatrixClient, debug, log):
        if data is None:
            raise AssertionError("data cannot be None")
        self.client = matrix_client
        self.data = data
        self.debug = debug
        self.log = log
        self.urlDispatcher = UrlDispatcher()
        callable0 = lambda request: log.info('lambda called')
        route = self.urlDispatcher.add_route('GET', self.pathPrefix + '/ci-job/{project}/{job}',
                                             callable0)
        exec_lambda = lambda request: handle_request(self, request)
        self.urlDispatcher.add_route('GET', self.pathPrefix + '/eval/{expression:.*}', exec_lambda)

    def user(self, mxid: str):
        users = self.data.ram['users']
        if mxid not in users:
            users[mxid] = Data.UserInformation()
        return users[mxid]

    async def get_user_information(self, mxid):
        user_information = self.user(mxid)
        if user_information.name is None:
            user_information.name = await self.client.get_displayname(mxid)
        return user_information

    async def unacknowledge(self, event):
        pass
        # if 'acknowledge' in self.debug:
        #     _acknowledge = self.debug['acknowledge']
        #     if 'react' in _acknowledge:
        #         await event.react()

    async def acknowledge(self, event):
        if 'acknowledge' in self.debug:
            _acknowledge = self.debug['acknowledge']
            if 'react' in _acknowledge:
                await event.react(_acknowledge['react'])

    async def perform_action(self, action: Action, arguments: dict):

        self.log.info(f"Performing Action '{action.id}' with arguments : {arguments}")

        for command in action.commands:
            command_arguments = {}
            if isinstance(command, Action.MatrixRoomMessageCommand):
                relates_to = None
                if command.relate_to_event is not None:
                    relates_to=RelatesTo(
                        rel_type=RelationType.REPLY,
                        event_id=arguments[command.relate_to_event].event_id,
                    )

                room = arguments[command.room]
                if isinstance(room, Data.Room):
                    room = room.mxid
                command_arguments['room'] = room

                command_arguments['message'] = arguments[command.message]

                self.log.info(f"Command arguments : {command_arguments}")


                content = TextMessageEventContent(
                    msgtype=MessageType.NOTICE, format=Format.HTML,
                    body=command_arguments['message'],
                    formatted_body=command_arguments['message'],
                    relates_to=relates_to)
                if command.relate_to_event is not None:
                    content.set_reply(arguments[command.relate_to_event])
                await self.client.send_message(command_arguments['room'], content)


    async def handle_room_reaction(self, reaction_event: ReactionEvent):

        if reaction_event.sender == self.client.mxid:
            return

        content = reaction_event.content
        content_key = content.relates_to.key

        event_prototype = None
        for possible_event_prototype in self.data.events:
            if possible_event_prototype.type == Event.Type.MATRIX_REACTION:
                if possible_event_prototype.matrix_reaction == content_key:
                    event_prototype = possible_event_prototype
                    break

        if event_prototype is None:
            self.log.debug(f"No match for room message '{content_key}'")
            return

        self.log.info(f"handle_room_reaction : {content_key}")

    async def handle_room_message(self, message_event: MessageEvent):

        if message_event.sender == self.client.mxid:
            return

        content = message_event.content
        content_raw = content.body

        event_prototype = None
        for possible_event_prototype in self.data.events:
            if possible_event_prototype.type == Event.Type.MATRIX_COMMAND:
                if content_raw == f"!{possible_event_prototype.matrix_command}" or content_raw.startswith(f"!{possible_event_prototype.matrix_command} "):
                    event_prototype = possible_event_prototype
                    break
            elif possible_event_prototype.type == Event.Type.MATRIX_MESSAGE_REGEX:
                if re.search(possible_event_prototype.matrix_message_matcher, content_raw):
                    event_prototype = possible_event_prototype
                    break

        if event_prototype is None:
            self.log.debug(f"No match for room message '{content_raw}'")
            return

        event = event_prototype.invocation()
        sender_id = next(filter(lambda u: u.mxid == message_event.sender, self.data.users), None)
        event.sender = Data.User(sender_id, message_event.sender)
        room_id = next(filter(lambda r: r.mxid == message_event.room_id, self.data.rooms), None)
        event.room = Data.Room(room_id, message_event.room_id)
        event.event_message = message_event

        if event_prototype.type == Event.Type.MATRIX_COMMAND:
            parameters_line = content_raw[len(event_prototype.matrix_command)+2:]
            parameters_indexed = shlex.split(parameters_line)
            # TODO named parameters
            parameters_named = {}
            event.matrix_command_arguments = Event.Invocation.Arguments(parameters_line, parameters_indexed, parameters_named)
        event.message = content_raw

        # event.set_fully_read_marker()

        await self.acknowledge(message_event)

        # sender_information = await self.get_user_information(message_event.sender)

        # self.log.info(f"event.command_arguments = {event.command_arguments}")

        actuator_index = 0
        actuator = None
        actuators_with_matching_action_exist = False
        mappers = None
        for possible_actuator in self.data.actuators:
            if possible_actuator.action == event.prototype.action:
                actuators_with_matching_action_exist = True
                for injector_type, possible_mappers in possible_actuator.injectors.items():
                    if event_prototype.type.name == injector_type:
                        mappers = possible_mappers
                        break
                if mappers is not None:
                    break
            actuator_index += 1

        if mappers is None:
            # if actuators_with_matching_action_exist:
            #     log
            # else:
            #     log
            return

        action = None
        try:
            action = next(filter(lambda a: a.id == event.prototype.action, self.data.actions))
        except StopIteration as ex:
            self.log.error(f"No action with id {event.prototype.action}")
            return

        mapper_context = {}

        if event_prototype.type == Event.Type.MATRIX_COMMAND \
                or event_prototype.type == Event.Type.MATRIX_MESSAGE_REGEX:
            mapper_context['matrix'] = EvaluationContext()
            mapper_context['matrix'].sender = event.sender
            mapper_context['matrix'].room = event.room
            if event_prototype.type == Event.Type.MATRIX_COMMAND:
                mapper_context['command'] = EvaluationContext()
                mapper_context['command'].name = event_prototype.matrix_command
                mapper_context['command'].argument = event.matrix_command_arguments

        action_arguments = {}
        one_or_more_arguments_failed = False
        for parameter in action.parameters:
            mapper = next(filter(lambda m: m.target == parameter.name, mappers), None)

            argument_value = None

            if mapper is None:
                automatically_provided = False
                if parameter.automatic_context:
                    if parameter.kind == 'matrix.room':
                        argument_value = event.room
                        automatically_provided = True
                        self.log.debug(f"Using automatic contextual value '{argument_value}' for '{parameter.name}' of type '{parameter.kind}'")
                    elif parameter.kind == 'matrix.message':
                        argument_value = event.event_message
                        automatically_provided = True
                        self.log.debug(f"Using automatic contextual value '{argument_value}' for '{parameter.name}' of type '{parameter.kind}'")
                    else:
                        self.log.warning(f"No automatic contextual value of type '{parameter.kind}' for '{parameter.name}'")
                        pass

                if not automatically_provided:
                    if parameter.mandatory:
                        continue
                    else:
                        self.log.error(f"No mapper nor automatic contextual value for '{parameter.name}'")
                        one_or_more_arguments_failed = True
                        continue
            else:
                try:
                    argument_value = execute_expression(mapper.source, mapper_context)
                    self.log.debug(f"Computed value for '{parameter.name}' is : '{argument_value}' ")
                except BaseException as e:
                    one_or_more_arguments_failed = True
                    self.log.error(f"Failed to copy-evaluate parameter '{parameter.name}' : "+str(e))
                    continue

            if parameter.evaluate:
                try:
                    argument_value = execute_expression(argument_value, {})
                    self.log.debug(f"On demande evaluation for '{parameter.name}' is : '{argument_value}' ")
                    if not isinstance(argument_value, str):
                        argument_value = str(argument_value)
                except BaseException as e:
                    one_or_more_arguments_failed = True
                    self.log.error(f"Failed to on-demande evaluate parameter '{parameter.name}' : "+str(e))
                    continue

            action_arguments[parameter.name] = argument_value

        if one_or_more_arguments_failed:
            self.log.error(f"Invalid parameters for action '{action.id}'")
            return

        await self.perform_action(action, action_arguments)

        await self.unacknowledge(message_event)


    async def handle_webhook_event(self, event: str, request: Request):
        self.log.info(f"Processing event '{event}'")
        match_info = await self.urlDispatcher.resolve(request)
        project_id = match_info.get('project')
        job_id = match_info.get('job')
        self.log.info(f"event data '{project_id}'/'{job_id}'")
        try:
            return await match_info.handler(request)
        except (HTTPNotFound, HTTPMethodNotAllowed) as ex:
            return generic_response(request, status=404, title='Not found / Method not allowed')
