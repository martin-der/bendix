from enum import Enum
from mautrix.types import EventType, TextMessageEventContent, MessageType, Format, RelatesTo, RelationType
from maubot.client import MaubotMatrixClient
import traceback


class BendixLogger:

    class Severity(Enum):
        CRITICAL = 50
        FATAL = CRITICAL
        ERROR = 40
        WARNING = 30
        WARN = WARNING
        SUCCESS = 21
        INFO = 20
        DEBUG = 10
        NOTSET = 0

    rooms: []

    backendLogger: object
    client: MaubotMatrixClient

    def __init__(self, client: 'MaubotMatrixClient', logger=None):
        self.backendLogger = logger
        self.client = client
        self.rooms = []

    async def _log(self, msg: str, severity: Severity, context: str = None, error: Exception = None):
        prefix = ''
        if severity == BendixLogger.Severity.CRITICAL:
            prefix = '🕸️ '
        elif severity == BendixLogger.Severity.ERROR:
            prefix = '🌶️ '
        elif severity == BendixLogger.Severity.WARN or severity == BendixLogger.Severity.WARNING:
            prefix = '🍌 '
        elif severity == BendixLogger.Severity.INFO:
            prefix = '🐬 '
        elif severity == BendixLogger.Severity.SUCCESS:
            prefix = '🐉 '
        elif severity == BendixLogger.Severity.DEBUG:
            prefix = '[🪲] '
        else:
            prefix = '[⁉️] '

        content = TextMessageEventContent(msgtype=MessageType.TEXT, format=Format.HTML)

        content.formatted_body = f"{prefix}{msg}"
        content.body = f"{prefix}{msg}"
        if error is not None:
            stack_lines = traceback.format_exception(etype=type(error), value=error, tb=error.__traceback__)
            clean_lines = [s for s in stack_lines if s.strip()]
            content.body = content.body + "\n" + ''.join(clean_lines)
            content.formatted_body = content.formatted_body + "<br/>" + '<pre><code>'+''.join(clean_lines)+'</code></pre>'
        log_content = content["net.tetrakoopa.bendix.log"] = {
            "severity": str(severity)
        }
        if context is not None:
            log_content['context'] = context

        for room in self.rooms:
            await self.client.send_message(room, content)

    async def debug(self, msg: str, context: str = None, error: Exception = None):
        await self._log(msg, BendixLogger.Severity.DEBUG, context=context, error=error)

    async def info(self, msg: str, context: str = None, error: Exception = None):
        await self._log(msg, BendixLogger.Severity.INFO, context=context, error=error)

    async def success(self, msg: str, context: str = None, error: Exception = None):
        await self._log(msg, BendixLogger.Severity.SUCCESS, context=context, error=error)

    async def warning(self, msg: str, context: str = None, error: Exception = None):
        await self.warn(msg, context=context, error=error)

    async def warn(self, msg: str, context: str = None, error: Exception = None):
        await self._log(msg, BendixLogger.Severity.WARN, context=context, error=error)

    async def error(self, msg: str, context: str = None, error: Exception = None):
        await self._log(msg, BendixLogger.Severity.ERROR, context=context, error=error)

    async def critical(self, msg: str, context: str = None, error: Exception = None):
        await self._log(msg, BendixLogger.Severity.ERROR, context=context, error=error)
