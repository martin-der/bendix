from circuit import Event, Actuator, Action
from mother_board import Data

class ConfigException(Exception):

    def __init__(self, message: str = None):
        super().__init__(message)


def load_config(bendix_bot: object, in_config):

    def load_debug(config):
        result = {}
        debug = config.get('debug', None)
        if debug is not None:
            if 'log' in debug:
                result['log'] = {}
                log_config = debug.get('log')
                if 'room' in log_config:
                    result['log']['room'] = log_config.get('room')
            if 'acknowledge' in debug:
                result['acknowledge'] = {}
                acknowledge = debug.get('acknowledge')
                if 'react' in acknowledge:
                    result['acknowledge']['react'] = acknowledge.get('react')
        return result

    def matching_event_type(name: str):
        for t in Event.Type:
            if name.upper().replace('.', '_') == t.name:
                return Event.Type[t.name]
        return None

    def load_event_config(event_config, event_id):
        if 'action' not in event_config:
            raise ConfigException(f"[Event] Event #{event_id} doesn't define 'action'")

        if 'webhook-path' in event_config:
            event = Event(str(event_id), Event.Type.WEB_HOOK)
            event.webhook_path = event_config.get('webhook-path')
        elif 'command' in event_config:
            event = Event(str(event_id), Event.Type.MATRIX_COMMAND)
            parts_raw = event_config.get('command')
            if not isinstance(parts_raw, str):
                raise ConfigException(f"[Event] Event #{event_id} command must be a string")
            parts = parts_raw.split()
            if len(parts) == 0:
                raise ConfigException(f"[Event] Event #{event_id} invalid command : '{parts_raw}'")
            event.matrix_command = parts[0]
            event.matrix_command_parameters = parts[1:]
        elif 'message' in event_config:
            event = Event(str(event_id), Event.Type.MATRIX_MESSAGE_REGEX)
            event.matrix_message_matcher = event_config.get('message')
        elif 'reaction' in event_config:
            event = Event(str(event_id), Event.Type.MATRIX_REACTION)
            event.matrix_reaction = event_config.get('reaction')
        else:
            raise ConfigException(f"[Event] Could not figure out event kind")

        event.action = event_config.get('action')

        return event

    def load_action_command_config(command_config):

        if 'type' in command_config:
            type_name = command_config.get('type')
            if type_name == 'http_request':
                method = command_config.get('method', None)
                if 'url' not in command_config:
                    raise ConfigException("[Action.HttpRequestCommand] Expected an URL ('url')")
                    command.url = command_config.get('url')
                command = Action.HttpRequestCommand(url, method)
            elif type_name == 'matrix_command':
                command = Action.MatrixRoomMessageCommand()
                if 'message' not in command_config:
                    raise ConfigException("[Action.MatrixRoomMessageCommand] Expected a message ('message')")
                command.message = command_config.get('message')
                if 'user' in command_config:
                    command.user = command_config.get('user')
                if 'room' in command_config:
                    command.room = command_config.get('room')
                if 'relate_to' in command_config:
                    command.relate_to_event = command_config.get('relate_to')
                if not (command.room or command.user):
                    raise ConfigException("[Action.MatrixRoomMessageCommand] Expected a room id a user id ('room', 'user')")
            else:
                raise ConfigException(f"[Action.Command] Not a valid command type '{type_name}'")

            return command

        if 'url' in command_config:
            url = command_config.get('url')
            method = command_config.get('method', None)
            command = Action.HttpRequestCommand(url, method)

            return command

        if 'room' in command_config or 'user' in command_config:
            command = Action.MatrixRoomMessageCommand()
            if 'message' not in command_config:
                raise ConfigException("[Action.MatrixRoomMessageCommand] Expected a message ('message')")
            command.message = command_config.get('message')
            if 'user' in command_config:
                command.user = command_config.get('user')
            if 'room' in command_config:
                command.room = command_config.get('room')
            if 'relate_to' in command_config:
                command.relate_to_event = command_config.get('relate_to')
            if command.room is not None and command.user is not None:
                raise ConfigException("[Action.MatrixRoomMessageCommand] Expected a room or a user but not both ('user', 'room')")
            return command

        raise ConfigException(f"[Action.Command] Could not figure out command kind")



    bendix_bot.data = Data()

    projects = in_config.get('projects', {})
    for project_id, config in projects.items():
        project = Data.Project(project_id)
        if 'name' in config:
            project.name = config.get('name')
        if project.name is None or project.name == '':
            project.name = project_id
        bendix_bot.data.projects.append(project)

    rooms = in_config.get('rooms', {})
    for room_id, config in rooms.items():
        room = Data.Room(room_id)
        if isinstance(config, str):
            room.mxid = config
        else:
            if 'mxid' in config:
                room.mxid = config.get('mxid')
        bendix_bot.data.rooms.append(room)

    rights = in_config.get('rights', [])
    for config in rights:
        right = Data.Right(config)
        bendix_bot.data.rights.append(right)

    users = in_config.get('users', {})
    for user_id, config in users.items():
        user = Data.User(user_id)
        if 'mxid' in config:
            user.mxid = config.get('mxid')
        if 'rights' in config:
            rights = config.get('rights', [])
            for right_config in rights:
                right = Data.Right(right_config)
                user.rights.append(right)
        bendix_bot.data.users.append(user)

    events = in_config.get('events', [])
    event_id = 1
    for config in events:
        bendix_bot.data.events.append(load_event_config(config, event_id))
        event_id = event_id + 1

    actuators = in_config.get('actuators', [])
    actuator_id = 1
    for config in actuators:
        actuator = Actuator(str(actuator_id))
        actuator_id = actuator_id + 1
        if 'name' in config:
            actuator.name = config.get('name')
        if 'action' in config:
            action_id = config.get('action')
            # if len(list(filter(lambda a: a.id == action_id, bendix_bot.data.actions))):
            actuator.action = action_id
            # else:
            #     pass  # error f"Unknown action {action_id}"
        if 'injectors' in config:
            injectors = config.get('injectors')
            for injector_type, injector_config in injectors.items():
                met = matching_event_type(injector_type)
                if met is not None:
                    injector = []
                    for map_config in injector_config:
                        map = Data.Mapper()
                        map.source = map_config.get('source')
                        map.target = map_config.get('target')
                        injector.append(map)
                    actuator.injectors[str(met.name)] = injector
                else:
                    # log bad event type
                    pass

        bendix_bot.data.actuators.append(actuator)

    actions = in_config.get('actions', {})
    for action_id, config in actions.items():
        action = Action(action_id)
        if 'name' in config:
            action.name = config.get('name')
        if 'parameters' in config:
            parameters_config = config.get('parameters')
            for parameter_name, parameter_config in parameters_config.items():
                parameter_evaluate_by_name = False
                if parameter_name.endswith('%'):
                    parameter_evaluate_by_name = True
                    parameter_name = parameter_name[:-1]
                if isinstance(parameter_config, str):
                    parameter_kind = parameter_config
                    parameter_mandatory = True
                    parameter_automatic = None
                    parameter_evaluate = parameter_evaluate_by_name
                else:
                    parameter_kind = parameter_config.get('type', 'stringzz')
                    parameter_mandatory = parameter_config.get('mandatory', True)
                    parameter_automatic = parameter_config.get('automatic', None)
                    parameter_evaluate = parameter_config.get('evaluate', parameter_evaluate_by_name)
                    if isinstance(parameter_automatic, bool):
                        parameter_automatic = '*' if parameter_automatic else None
                parameter = Action.Parameter(parameter_name, parameter_kind, parameter_mandatory, parameter_automatic, parameter_evaluate)
                action.parameters.append(parameter)
        if 'command' in config:
            command_config = config.get('command')
            action.commands.append(load_action_command_config(command_config))
        bendix_bot.data.actions.append(action)

    bendix_bot.debug = load_debug(in_config)
