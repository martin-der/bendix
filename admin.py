from maubot.handlers import web
from aiohttp.web import Request, Response, json_response
from circuit import Event, Action
from mother_board import Data
from resources import Resources
from web import generic_response

def col2json(elements, to_json):
    result = []
    for element in elements:
        result.append(to_json(element))
    return result

def dict2json(dic, key_to_json, value_to_json):
    result = {}
    for k, v in dic.items():
        k_j = str(k) if key_to_json is None else str(key_to_json(k))
        result[k_j] = value_to_json(v)
    return result


class AdminHandler:

    data: Data
    debug: object

    def __init__(self, data: Data, debug):
        self.data = data
        self.debug = debug

    @staticmethod
    def project_to_json(project):
        return {'id': project.id, 'name': project.name}

    @staticmethod
    def projects_to_json(projects):
        result = []
        for project in projects:
            result.append(AdminHandler.project_to_json(project))
        return result

    @staticmethod
    def room_to_json(room):
        return {'id': room.id, 'mxid': room.mxid}

    @staticmethod
    def rooms_to_json(rooms):
        result = []
        for room in rooms:
            result.append(AdminHandler.room_to_json(room))
        return result

    @staticmethod
    def right_to_json(right):
        return {'id': right.id}

    @staticmethod
    def rights_to_json(rights):
        result = []
        for right in rights:
            result.append(AdminHandler.right_to_json(right))
        return result

    @staticmethod
    def user_to_json(user):
        rights = AdminHandler.rights_to_json(user.rights)
        return {'id': user.id, 'mxid': user.mxid, 'rights': rights}

    @staticmethod
    def users_to_json(users):
        result = []
        for user in users:
            result.append(AdminHandler.user_to_json(user))
        return result

    @staticmethod
    def event_to_json(event):
        result = {'id': event.id, 'action': event.action, 'type': str(event.type.name)}
        if event.type == Event.Type.WEB_HOOK:
            return {**result, **{'method': event.method, 'path': event.webhook_path}}
        elif event.type == Event.Type.MATRIX_COMMAND:
            return {**result, **{'command': event.matrix_command, 'parameters': event.matrix_command_parameters}}
        elif event.type == Event.Type.MATRIX_MESSAGE_REGEX:
            return {**result, **{'matcher': event.matrix_message_matcher}}
        elif event.type == Event.Type.MATRIX_REACTION:
            return {**result, **{'reaction': event.matrix_reaction}}
        return result

    @staticmethod
    def events_to_json(events):
        result = []
        for event in events:
            result.append(AdminHandler.event_to_json(event))
        return result

    @staticmethod
    def actuator_injector_mapping_to_json(mapping):
        return {'source': mapping.source, 'target': mapping.target}

    @staticmethod
    def actuator_injector_to_json(value):
        return col2json(value, AdminHandler.actuator_injector_mapping_to_json)

    @staticmethod
    def actuator_to_json(actuator):
        return {'id': actuator.id, 'name': actuator.name,
                'action': actuator.action,
                'injectors': dict2json(actuator.injectors, None, AdminHandler.actuator_injector_to_json)}

    @staticmethod
    def action_parameter_to_json(parameter):
        return {'name': parameter.name, 'kind': parameter.kind,
                'mandatory': parameter.mandatory,
                'automatic': parameter.automatic_context,
                'evaluate': parameter.evaluate}

    @staticmethod
    def action_command_to_json(command):
        common = {'kind': command.kind }
        if isinstance(command, Action.HttpRequestCommand):
            return common | {'url': command.url, 'method': command.method}
        if isinstance(command, Action.MatrixRoomMessageCommand):
            return {**common, **{'message': command.message,
                                 'user': command.user,
                                 'room': command.room,
                                 'relate_to_event': command.relate_to_event}}
        return common

    @staticmethod
    def action_to_json(action):
        return {'id': action.id, 'name': action.name,
                'enabled': action.enabled,
                'parameters': col2json(action.parameters, AdminHandler.action_parameter_to_json),
                'commands': col2json(action.commands, AdminHandler.action_command_to_json)}

    @staticmethod
    def actions_to_json(actions):
        result = []
        for action in actions:
            result.append(AdminHandler.action_to_json(action))
        return result

    @web.get("/admin")
    async def admin_main(self, request: Request) -> Response:
        return Response(content_type='text/html', charset='UTF-8',
                        body=Resources.string('web/admin/admin.xhtml'))

    @web.get("/admin/status")
    async def admin_status(self, request: Request) -> Response:
        problems = []
        return json_response({"status": 'ok', 'problems': problems})

    @web.put("/admin/config")
    async def admin_config_update(self, request: Request) -> Response:
        return json_response({"status": 'ok'})

    @web.get("/admin/config")
    async def admin_config_retrieve(self, request: Request) -> Response:
        config = {
            'projects': AdminHandler.projects_to_json(self.data.projects),
            'rooms': AdminHandler.rooms_to_json(self.data.rooms),
            'rights': AdminHandler.rights_to_json(self.data.rights),
            'users': AdminHandler.users_to_json(self.data.users),
            'events': AdminHandler.events_to_json(self.data.events),
            'actuators': col2json(self.data.actuators, AdminHandler.actuator_to_json),
            'actions': AdminHandler.actions_to_json(self.data.actions),
        }
        return generic_response(request, 'Config', {'config': config})

    @web.get("/admin/rooms")
    async def admin_rooms_retrieve(self, request: Request) -> Response:
        return generic_response(request, 'Rooms', {"rooms": AdminHandler.rooms_to_json(self.data.rooms)})

    @web.get("/admin/room/{room}")
    async def admin_room_retrieve(self, request: Request) -> Response:
        room_id = request.match_info.get("room")
        for room in self.data.rooms:
            if room.id == room_id:
                return generic_response(request, 'Room', AdminHandler.room_to_json(room))
        message = f"No such room '{room_id}"
        return Response(status=404, reason=message, body=message)

    @web.get("/admin/projects")
    async def admin_projects_retrieve(self, request: Request) -> Response:
        return generic_response(request, 'Projects', {"projects": AdminHandler.projects_to_json(self.data.projects)})

    @web.get("/admin/project/{project}")
    async def admin_project_retrieve(self, request: Request) -> Response:
        project_id = request.match_info.get("project")
        for project in self.data.projects:
            if project.id == project_id:
                return generic_response(request, 'Project', AdminHandler.project_to_json(project))
        message = f"No such project '{project_id}"
        return Response(status=404, reason=message, body=message)

    @web.get("/admin/rights")
    async def admin_right(self, request: Request) -> Response:
        return generic_response(request, 'Rights', {"rights": AdminHandler.rights_to_json(self.data.rights)})

    @web.get("/admin/users")
    async def admin_users_retrieve(self, request: Request) -> Response:
        return generic_response(request, 'Users', {"users": AdminHandler.users_to_json(self.data.users)})

    @web.get("/admin/user/{user}")
    async def admin_user_retrieve(self, request: Request) -> Response:
        user_id = request.match_info.get("user")
        for user in self.data.users:
            if user.id == user_id:
                return generic_response(request, 'User', AdminHandler.user_to_json(user))
        message = f"No such user '{user_id}"
        return Response(status=404, reason=message, body=message)

    @web.put("/admin/user/{user}")
    async def admin_user_update(self, request: Request) -> Response:
        user_id = request.match_info.get("user")
        return generic_response(request, 'User updated', {"status": 'ok', 'updated': user_id})

    @web.post("/admin/user/{user}")
    async def admin_user_create(self, request: Request) -> Response:
        user_id = request.match_info.get("user")
        return generic_response(request, 'User created', {"status": 'ok', 'created': user_id})

    @web.delete("/admin/user/{user}")
    async def admin_user_delete(self, request: Request) -> Response:
        user_id = request.match_info.get("user")
        return generic_response(request, 'User deleted', {"status": 'ok', 'created': user_id})

    @web.post("/admin/user/right/{right}")
    async def admin_user_right_add(self, request: Request) -> Response:
        right = request.match_info.get("right")
        return generic_response(request, 'Right added', {"status": 'ok', 'added': right})

    @web.get("/admin/actuators")
    async def admin_actuators_retrieve(self, request: Request) -> Response:
        return generic_response(request, 'Actuators', {
            "actuators": col2json(self.data.actuators, AdminHandler.actuator_to_json)
        })

    @web.get("/admin/events")
    async def admin_events_retrieve(self, request: Request) -> Response:
        return generic_response(request, 'Events', {
            "events": AdminHandler.events_to_json(self.data.events)
        })

    @web.get("/admin/actions")
    async def admin_actions_retrieve(self, request: Request) -> Response:
        return generic_response(request, 'Actions', {
            "actions": AdminHandler.actions_to_json(self.data.actions)
        })

    @web.get("/admin/action/{action}")
    async def admin_action_retrieve(self, request: Request) -> Response:
        action_id = request.match_info.get("action")
        for action in self.data.actions:
            if action.id == action_id:
                return generic_response(request, 'Actions', AdminHandler.action_to_json(action))
        message = f"No such action '{action_id}"
        return Response(status=404, reason=message, body=message)

    @web.get("/admin/debug")
    async def admin_debug_retrieve(self, request: Request) -> Response:
        return generic_response(request, 'Debug', {
            "debug": self.debug
        })

