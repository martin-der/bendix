
class Data:

    types = {
        'string': 'string',
        'real': 'real',
        'integer': 'integer',

        'matrix': {
            'room': 'matrix.room',
            'user': 'matrix.room'
        }
    }

    class Mapper:
        source: str
        target: str

    class Project:

        id: str

        name: str

        def __init__(self, id: str):
            self.id = id
            self.name = None
            self.parameters = []

    class Room:
        id: str

        mxid: str

        def __init__(self, id: str, mxid: str = None):
            self.id = id
            self.mxid = mxid

    class Right:
        id = None

        def __init__(self, id: str):
            self.id = id

    class User:
        id: str

        mxid: str

        rights: []

        def __init__(self, id: str, mxid: str = None):
            self.id = id
            self.mxid = mxid
            self.rights = []

    def get_room_by_mxid(self, room_mxid):
        try:
            return next(filter(lambda r: r.mxid == room_mxid, self.rooms))
        except StopIteration as ex:
            return None

    def get_room_mxid_by_id(self, room_id):
        try:
            return next(filter(lambda r: r.id == room_id, self.rooms)).mxid
        except StopIteration as ex:
            return None

    def get_user_by_mxid(self, user_mxid):
        try:
            return next(filter(lambda u: u.mxid == user_mxid, self.users))
        except StopIteration as ex:
            return None

    projects: []

    rooms: []

    rights: []

    users: []

    events: []

    actions: []

    actuators: []

    ram: dict

    def __init__(self):
        self.projects = []
        self.rooms = []
        self.rights = []
        self.users = []
        self.events = []
        self.actions = []
        self.actuators = []
        self.ram = {
            'users': {}
        }
