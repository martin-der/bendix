
cd "$(dirname "$0")"

TARGET=out/bendix.mbp

python3 -m maubot.cli build -o "${TARGET}"

#rm -f "${TARGET}" || exit 1
#zip -9qr "${TARGET}" bendix.py admin.py maubot.yaml base-config.yaml || exit 1
echo "Made '${TARGET}' (on $(date +%s))"