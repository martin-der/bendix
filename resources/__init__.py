import pkg_resources

resource_package = __name__

content_types = {
    'otf': 'font/otf',
    'ttf': 'font/ttf',
    'woff': 'font/woff',
    'woff2': 'font/woff2',
    'svg': 'image/svg+xml'
}

class Resources:

    @staticmethod
    def b_string(path: str):
        global resource_package
        return pkg_resources.resource_string(resource_package, path)

    @staticmethod
    def string(path: str):
        global resource_package
        return pkg_resources.resource_string(resource_package, path).decode("utf-8")

    @staticmethod
    def content_type(extension: str):
        if extension in content_types:
            return content_types[extension]

        return'application/octet-stream'
