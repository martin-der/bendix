
const default_api_headers = {
	"Content-Type": "application/json"
}

const api_path = 'http://localhost:29316/_matrix/maubot/plugin/sb2/event'

const Admin = {

  data() {
    return {
      counter: 0,
      projects : [],
      users : [],
      rooms : [],
      actions : [],
      actuators : [],
      events : []
    }
  },

  methods: {
  	simulate_event(id) {
  		const event = this.events.find(e => e.id == id)
  		if (event == null) {
	  		console.log("Not such event with id '"+id+"'")
	  		return;
  		}
		console.log("Trigger event '"+id+"'")
		var path = event.path
		path = path.replace('{expression}', '2+3')
		fetch(api_path+path, {headers: default_api_headers})
			.then(res => res.json())
			.then(res => {
			  console.log(res);
			})
  	},
  	run_action(id) {
  		const action = this.actions.find(a => a.id == id)
  		if (action == null) {
	  		console.log("Not such action with id '"+id+"'")
	  		return;
  		}
		console.log("Trigger action '"+id+"'")
  	},
  	delete_action(id) {
  		const action = this.actions.find(a => a.id == id)
  		if (action == null) {
	  		console.log("Not such action with id '"+id+"'")
	  		return;
  		}
		console.log("Delete action '"+id+"'")
  	}
  },

  created:function() {

    fetch('./admin/rooms', {headers: default_api_headers})
    .then(res => res.json())
    .then(res => {
      this.rooms = res.content.rooms;
    })

    fetch('./admin/users', {headers: default_api_headers})
    .then(res => res.json())
    .then(res => {
      this.users = res.content.users;
    })

    fetch('./admin/projects', {headers: default_api_headers})
    .then(res => res.json())
    .then(res => {
      this.projects = res.content.projects;
    })

    fetch('./admin/events', {headers: default_api_headers})
    .then(res => res.json())
    .then(res => {
      this.events = res.content.events;
    })

    fetch('./admin/actuators', {headers: default_api_headers})
    .then(res => res.json())
    .then(res => {
      this.actuators = res.content.actuators;
    })

    fetch('./admin/actions', {headers: default_api_headers})
    .then(res => res.json())
    .then(res => {
      this.actions = res.content.actions;
    })

  }
}


