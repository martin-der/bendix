const EventTypeComponent = {
  props: {
    type: String,
  },
  template: `
  		<span class="injector-header">
			<span class="inner" v-if="type == 'WEB_HOOK'"><img src="./~/icon/google/http.svg"/></span>
			<span class="inner" v-if="type.startsWith('MATRIX_')">
				<img src="./~/icon/google/data_array.svg"/>
				<img v-if="type == 'MATRIX_MESSAGE_REGEX'" src="./~/icon/google/notes.svg"/>
				<img v-if="type == 'MATRIX_COMMAND'" src="./~/icon/google/computer.svg"/>
				<img v-if="type == 'MATRIX_REACTION'" src="./~/icon/google/emoji_emotions.svg"/>
			</span>
		</span>`
}

